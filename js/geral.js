
$(function(){


	
	

	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	$(document).ready(function() {
		var w = window.innerWidth;
		//CARROSSEL DE DESTAQUE
		if (w>991){
		$("#carrosselDestaque").owlCarousel({
			items : 2,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			margin: 25,
			center:true,
			animateOut: 'fadeOut',


		});
	}else{
		$("#carrosselDestaque").owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			margin: 25,
			animateOut: 'fadeOut',
			});
	}
		var carrossel_destaque = $("#carrosselDestaque").data('owlCarousel');
		$('#btnCarrosselDestaqueLeft').click(function(){ carrossel_destaque.prev(); });
		$('#btnCarrosselDestaqueRight').click(function(){ carrossel_destaque.next(); });

		//CARROSSEL DE DESTAQUE
		$("#carrosselDepoimentos").owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:false,
			touchDrag  : false,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			margin: 25,

		});
		var carrossel_depoimentos = $("#carrosselDepoimentos").data('owlCarousel');
		$('#btnCarrosselDepoimentosLeft').click(function(){ carrossel_depoimentos.prev(); });
		$('#btnCarrosselDepoimentosRight').click(function(){ carrossel_depoimentos.next(); });
		

		//CARROSSEL DE DESTAQUE
		$("#carrosselLogos").owlCarousel({
			items : 4,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			margin: 25,
			responsiveClass:true,			    
			        responsive:{
			            320:{
			                items:1
			            },
			            600:{
			                items:2
			            },
			           
			            991:{
			                items:3
			            },
			            1024:{
			                items:4
			            },
			            1440:{
			                items:4
			            },
			            			            
			        }	

		});
		
	});
	
});
